﻿using GalaSoft.MvvmLight;
using LDI.Application.Communication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LDI.Application.ViewModel
{
    public class MainViewModel:ViewModelBase
    {
        private CommunicationViewModel communicationViewModel;
        public CommunicationViewModel CommunicationViewModel
        {
            get
            {
                return communicationViewModel;
            }
        }


        

        public ComPortSettings ComSettings
        {
            get
            {
                return CommunicationViewModel.ComSettings;
            }
        }
        

        public MainViewModel()
        {
            communicationViewModel = new CommunicationViewModel();
        }

        private string status;

        public string Status
        {
            get { return status; }
            set
            {
                Set("Status", ref status, value);
            }
        }

        private bool isSettingsOpen;

        public bool IsSettingsOpen
        {
            get { return isSettingsOpen; }
            set
            {
                Set("IsSettingsOpen", ref isSettingsOpen, value);
            }
        }
    }
}
