﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using LDI.Application.Communication;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace LDI.Application.ViewModel
{
    public class CommunicationViewModel : ViewModelBase, IDisposable, ILogger
    {
        private ComPortSettings comPortSettings = new ComPortSettings();
        public ComPortSettings ComSettings
        {
            get
            {
                return comPortSettings;
            }
        }

        private string input;

        public string Input
        {
            get { return input; }
            set
            {
                Set("Input", ref input, value);
            }
        }

        private string consoleOutput;

        public string ConsoleOutput
        {
            get { return consoleOutput; }
            set
            {
                Set("ConsoleOutput", ref consoleOutput, value);
            }
        }

        private bool isSerial = true;
        public bool IsSerial
        {
            get
            {
                return isSerial;
            }
            set
            {
                Set("IsSerial", ref isSerial, value);
            }
        }

        private bool isTCP = false;
        public bool IsTCP
        {
            get
            {
                return isTCP;
            }
            set
            {
                Set("IsTCP", ref isTCP, value);
            }
        }

        public int portNumber = 8000;
        public int PortNumber
        {
            get
            {
                return portNumber;
            }
            set
            {
                Set("PortNumber", ref portNumber, value);
            }
        }



        private StringBuilder consoleBuilder = new StringBuilder();

        public ICommand ClearConsoleCommand
        {
            get
            {
                return new RelayCommand(ClearConsole);
            }
        }

        public ICommand CopyConsoleCommand
        {
            get
            {
                return new RelayCommand(CopyConsole);
            }
        }

        private void CopyConsole()
        {
            Clipboard.SetData(System.Windows.DataFormats.Text, ConsoleOutput);
        }

        public ICommand SendCommand
        {
            get
            {
                return new RelayCommand(Send);
            }
        }

        public ICommand RefreshSerialPort
        {
            get
            {
                return new RelayCommand(Refresh);
            }
        }

        private void Refresh()
        {
            ServiceLocator.Current.GetInstance<MainViewModel>().Status = string.Format("Bağlantı noktası olarak {0} ayarlandı.", comPortSettings.SelectedPortName);
            ServiceLocator.Current.GetInstance<MainViewModel>().IsSettingsOpen = false;
            CommunicationPort.Open();
        }

        private void ClearConsole()
        {
            consoleBuilder.Clear();
            ConsoleOutput = string.Empty;
        }

        private void AddToConsole(string text)
        {
            consoleBuilder.AppendLine(text);
            ConsoleOutput = consoleBuilder.ToString();

        }

        private async void Send()
        {
            if (!string.IsNullOrEmpty(Input))
            {
                await CommunicationPort.SendAsync(Constants.ConvertFormattedStringToRaw(Input));
                AddToConsole("OUTPUT :" + Input);
            }
        }

        private ICommunicationProtocol communicationPort;
        private ICommunicationProtocol CommunicationPort
        {
            get
            {
                if (communicationPort == null)
                {
                    if (IsTCP)
                        communicationPort = new TcpIpCommunication(PortNumber, this);
                    else
                        communicationPort = new SerialCommunication(this.ComSettings);
                    try
                    {
                        communicationPort.Open();
                        communicationPort.DataReceived += SerialPort_DataReceived;
                        ServiceLocator.Current.GetInstance<MainViewModel>().Status = "Communication set for " + communicationPort.Name;
                    }
                    catch (Exception ex)
                    {
                        ServiceLocator.Current.GetInstance<MainViewModel>().Status = "Error :" + ex.Message;
                    }
                }
                return communicationPort;
            }
        }

        private void SerialPort_DataReceived(object sender, CommunicationEventArgs e)
        {
            string data = e.Data;
            AddToConsole("INPUT  :" + Constants.FormatRawString(data));
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (CommunicationPort is IDisposable)
                    (CommunicationPort as IDisposable).Dispose();
            }
        }

        public void Log(string message)
        {
            ServiceLocator.Current.GetInstance<MainViewModel>().Status = message;
        }
    }
}
