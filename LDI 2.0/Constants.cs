﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LDI.Application
{
    public class Constants
    {
        public const char ENQ = '\x5';
        public const char STX = '\x2';
        public const char NACK = '\x15';
        public const char ETX = '\x3';
        public const char ACK = '\x6';
        public const char EOT = '\x4';
        public const char NUL = '\x0';
        public const char SOH = '\x1';
        public const char ETB = '\x17';
        public const char CR = '\xD';
        public const char LF = '\xA';
        public const char GS = '\x1D';
        public const char RS = '\x1E';
        public const char FS = '\x1C';
        public const char SYN = '\x16';
        public const char DLE = '\x10';
        public static Dictionary<int, string> ASCIISpecial = new Dictionary<int, string>()
        {
            { 0, "[NULL]"},
            { 1, "[SOH]"},
            { 2, "[STX]"},
            { 3, "[ETX]"},
            { 4, "[EOT]"},
            { 5, "[ENQ]"},
            { 6, "[ACK]"},
            { 7, "[BEL]"},
            { 8, "[BS]"},
            { 9, "[TAB]"},
            { 10, "[LF]"},
            { 11, "[VT]"},
            { 12, "[FF]"},
            { 13, "[CR]"},
            { 14, "[SO]"},
            { 15, "[SI]"},
            { 16, "[DLE]"},
            { 17, "[DC1]"},
            { 18, "[DC2]"},
            { 19, "[DC3]" },
            { 20, "[DC4]"},
            { 21, "[NAK]"},
            { 22, "[SYN]"},
            { 23, "[ETB]"},
            { 24, "[CAN]"},
            { 25, "[EM]"},
            { 26, "[SUB]"},
            { 27, "[ESC]"},
            { 28, "[FS]"},
            { 29, "[GS]"},
            { 30, "[RS]"},
            { 31, "[US]"},
        };

        public static string ConvertFormattedStringToRaw(string formatted)
        {
            foreach (var item in ASCIISpecial)
            {
                formatted = formatted.Replace(item.Value, ((char)item.Key).ToString());
            }
            return formatted;
        }

        public static string FormatRawString(string raw)
        {
            string formatted = raw;
            for (int i = 0; i < raw.Length; i++)
            {
                if (ASCIISpecial.ContainsKey(raw[i]))
                {
                    formatted = formatted.Replace(raw[i].ToString(), ASCIISpecial[i]);
                }
            }
            return formatted;
        }
    }
}
