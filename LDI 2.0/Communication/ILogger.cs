﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LDI.Application.Communication
{
    public interface ILogger
    {
        void Log(string message);
    }
}
