﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;

namespace LDI.Application.Communication
{
    public class ComPortSettings:GalaSoft.MvvmLight.ObservableObject
    {
        public ComPortSettings()
        {
        }

        private List<int> baudRates = new List<int>() {110, 300, 600, 1200, 2400, 4800, 9600, 14400, 19200, 28800, 38400, 56000, 57600,
            115200, 153600, 230400, 25600, 460800 };
        public List<int> BaudRates
        {
            get { return baudRates; }
        }

        private int selectedBaudRate = 9600;
        public int SelectedBaudRate
        {
            get { return selectedBaudRate; }
            set {
                selectedBaudRate = value;
                RaisePropertyChanged("SelectedBaudRate");
            }
        }

        private List<string> portNames = new List<string>() { "COM1", "COM2", "COM3", "COM4", "COM5", "COM6", "COM7", "COM8" };
        public List<string> PortNames
        {
            get
            {
                return portNames;
            }
        }

        private string selectedPortName = "COM1";
        public string SelectedPortName
        {
            get { return selectedPortName; }
            set
            {
                selectedPortName = value;
                RaisePropertyChanged("SelectedPortName");
            }
        }

        private int dataBits = 8;
        public int DataBits
        {
            get
            {
                return dataBits;
            }
            set
            {
                dataBits = value;
                RaisePropertyChanged("DataBits");
            }
        }

        private StopBits stopBits = StopBits.One;
        public StopBits StopBits
        {
            get
            {
                return stopBits;
            }
            set
            {
                stopBits = value;
                RaisePropertyChanged("StopBits");
            }
        }

        private bool dtrEnable = false;
        public bool DtrEnable
        {
            get
            {
                return dtrEnable;
            }
            set
            {
                dtrEnable = value;
                RaisePropertyChanged("DtrEnable");
            }
        }

        private bool rtsEnable = false;
        public bool RtsEnable
        {
            get
            {
                return rtsEnable;
            }
            set
            {
                rtsEnable = value;
                RaisePropertyChanged("RtsEnable");
            }
        }

        public SerialPort GetSerialPort()
        {
            SerialPort sp = new SerialPort();
            sp.BaudRate = SelectedBaudRate;
            sp.PortName = SelectedPortName;
            sp.DataBits = DataBits;
            sp.StopBits = this.StopBits;
            sp.DtrEnable = DtrEnable;
            sp.RtsEnable = RtsEnable;
            return sp;
        }


    }
}
