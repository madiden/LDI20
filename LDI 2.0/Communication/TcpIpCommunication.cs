﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace LDI.Application.Communication
{
    public class TcpIpCommunication : ICommunicationProtocol
    {
        TcpListener listener;
        TcpClient client;

        public ILogger Logger
        {
            get;set;
        }

        public string Name
        {
            get
            {
                return "TCP/IP";
            }
        }

        public TcpIpCommunication(int portNumber, ILogger logger)
        {
            Logger = logger;
            IPAddress ip = null;
            if (IPAddress.TryParse(GetLocalIpNumber(), out ip))
            {
                listener = new TcpListener(ip, portNumber);
            }
            else
            {
                throw new System.Net.ProtocolViolationException("Unable to fetch a valid IP address.");
            }

        }

        private async Task BeginListeningClient()
        {
            while (true)
            {
                client = await listener.AcceptTcpClientAsync();
                await Task.Factory.StartNew(ReadClient, TaskCreationOptions.LongRunning);
            }
        }

        private async void ReadClient()
        {
            NetworkStream sReader = client.GetStream();
            while (client.Connected)
            {
                if (sReader.CanRead && sReader.DataAvailable)
                {
                    using (StreamReader reader = new StreamReader(sReader))
                    {
                        string data = await reader.ReadToEndAsync();
                        if (DataReceived != null)
                            DataReceived(this, new CommunicationEventArgs(data));
                    }
                }
                await TaskEx.Delay(1000);
            }
        }

        private string GetLocalIpNumber()
        {
            using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, 0))
            {
                socket.Connect("10.0.2.4", 65530);
                IPEndPoint endPoint = socket.LocalEndPoint as IPEndPoint;
                return endPoint.Address.ToString();
            }
        }

        public event EventHandler<CommunicationEventArgs> DataReceived;

        public void Close()
        {
            if (listener != null)
            {
                listener.Stop();
                if (client != null && client.Connected)
                    client.Close();
            }
        }

        public void Open()
        {
            if (!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                throw new System.Net.ProtocolViolationException("There is no available connection.");
            }
            listener.Start();
            Task.Factory.StartNew(BeginListeningClient);
        }

        public async Task SendAsync(string data)
        {
            if (client.Connected)
            {
                NetworkStream wStream = client.GetStream();
                if (wStream.CanWrite)
                {
                    using (StreamWriter sw = new StreamWriter(wStream))
                    {
                        await sw.WriteAsync(data);
                    }
                }
            }else
            {
                Logger.Log("Client is not connected.");
            }
        }
    }
}
