﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDI.Application.Communication
{
    public class SerialCommunication : ICommunicationProtocol, IDisposable
    {
        private ComPortSettings settings;
        private SerialPort serialPort;

        
        ILogger ICommunicationProtocol.Logger
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public string Name
        {
            get
            {
                return "Serial";
            }
        }

        public event EventHandler<CommunicationEventArgs> DataReceived;

        public SerialCommunication(ComPortSettings _settings)
        {
            settings = _settings;
            serialPort = _settings.GetSerialPort();
            serialPort.DataReceived += SerialPort_DataReceived;
        }

        private void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            if (serialPort.IsOpen)
            {
                string data = serialPort.ReadExisting();
                if (DataReceived != null)
                    DataReceived(this, new CommunicationEventArgs(data));
            }
        }

        public void Close()
        {
            if (serialPort.IsOpen)
                serialPort.Close();
        }

        public void Open()
        {
            if (!serialPort.IsOpen)
                serialPort.Open();
        }      

        public async Task SendAsync(string data)
        {
            await Task.Factory.StartNew(()=>Send(data));
        }

        public void Send(string data)
        {
            if (serialPort.IsOpen)
                serialPort.Write(data);
        }

        public void Dispose()
        {
            if(serialPort != null)
            {
                if (serialPort.IsOpen)
                    serialPort.Close();
                serialPort.Dispose();
            }
        }
    }
}
