﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDI.Application.Communication
{
    interface ICommunicationProtocol
    {
        Task SendAsync(string data);
        void Open();
        void Close();
        event EventHandler<CommunicationEventArgs> DataReceived;
        ILogger Logger { get; set; }
        string Name { get; }
    }
}
