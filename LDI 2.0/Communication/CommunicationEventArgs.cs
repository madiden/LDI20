﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LDI.Application.Communication
{
    public class CommunicationEventArgs:EventArgs
    {
        private string data;
        public string Data
        {
            get { return data; }
        }
        public CommunicationEventArgs(string _data)
        {
            data = _data;
        }
    }
}
