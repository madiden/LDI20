﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LDI.Application.View
{
    /// <summary>
    /// Interaction logic for CommunicationResolverView.xaml
    /// </summary>
    public partial class CommunicationResolverView : UserControl
    {
        public CommunicationResolverView()
        {
            InitializeComponent();
            InitializeControls();
        }

        private void InitializeControls()
        {
            for (int x = 0; x < 4; x++)
            {
                StackPanel sp = new StackPanel();
                sp.HorizontalAlignment = HorizontalAlignment.Center;
                sp.Orientation = Orientation.Horizontal;
                for (int y = 0; y < 8; y++)
                {
                    Button b = new Button();
                    b.Width = 48; b.Height = 32;
                    b.Tag = x * 8 + y;
                    b.Margin = new Thickness(4);
                    b.Click += B_Click;
                    b.Content = Constants.ASCIISpecial[x*8 + y];
                    sp.Children.Add(b);
                }
                buttonsPanel.Children.Add(sp);
                DockPanel.SetDock(sp, Dock.Top);
            }
        }

        private void B_Click(object sender, RoutedEventArgs e)
        {
            int i = (int)((sender as Button).Tag);
            inputText.SelectedText = Constants.ASCIISpecial[i];
            inputText.CaretIndex += inputText.SelectedText.Length;
            inputText.SelectionLength = 0;
            inputText.Focus();
        }

        private void inputText_GotFocus(object sender, RoutedEventArgs e)
        {
            buttonsBorder.Visibility = Visibility.Visible;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            buttonsBorder.Visibility = Visibility.Collapsed;
        }        

        private void consoleChanged(object sender, TextChangedEventArgs e)
        {
            consoleOutput.ScrollToEnd();
        }
    }
}
